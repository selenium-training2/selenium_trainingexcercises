/*
2045657 - Jose Jorge Aguado Martinez
This is the Main Test of the First and Second cases, in the first one in the Test with valid credentials
and the second one with invalid password.
 */
package com.myStore.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class loginPage {

    WebDriver driver;
    //Three elements in the web page, username/email, password and the login button
    By emailAddress = By.id("email");
    By password = By.id("passwd");
    By loginButton = By.id("SubmitLogin");

    //Constructor of the class
    public loginPage(WebDriver driver){

        this.driver=driver;
    }

    //Method for enter the email address
    public void typeEmail(String email){

        driver.findElement(emailAddress).sendKeys(email);
    }

    //Method for enter a password
    public void typePassword(String passwd){

        driver.findElement(password).sendKeys(passwd);
    }

    //Method for login in the Account
    public void clickOnLoginButton(){

        driver.findElement(loginButton).click();
    }
}

