/*
2045657 - Jose Jorge Aguado Martinez
These are the First and Second cases, in the first one in the Test with valid credentials
and the second one with invalid password.
 */
package com.TestCases;

import com.myStore.pages.loginPage;
import com.myStore.pages.selectingPage;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class verifyLogin {
    /*
    First Test Case
    In this Test Case we have valid credentials (email address and password) to login in the web page
     */
    @Test
    public void verifyValidLogin(){

        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");

        //Create a loginPage object and executes three methods to test the valid login
        loginPage login= new loginPage(driver);
        login.typeEmail("jorge.aguado.martinez@gmail.com");
        login.typePassword("Demo123");
        login.clickOnLoginButton();

        //Quit the browser and finishes the Test

        driver.quit();
        System.out.println("First Test Case: Completed");
    }

    /*
   Second Test Case
   In this Test Case we have a valid email, but invalid password to login in the web page
   As the first test case we have the email, but a wrong password, so we cannot enter to the Account Page

    */
    @Test
    public void verifyInvalidLogin() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");

        loginPage login= new loginPage(driver);
        login.typeEmail("jorge.aguado.martinez@gmail.com");
        login.typePassword("demo123");
        login.clickOnLoginButton();

        //Printed info about invalid credentials and Second Test Case completed
        System.out.println("Cannot enter to the Account, please try again");
        System.out.println("Second Test Case: Completed");
        Thread.sleep(1000);
        driver.quit();
        System.out.println("First Scenario: Completed");
    }


}