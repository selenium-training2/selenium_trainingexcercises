/*
2045657 - Jose Jorge Aguado Martinez
This is the Main Test of the First and Second cases, in the first one is the test of the quick view for a window and
check the most relevant data of the product, the second one is about the more button for more information.
 */
package com.TestCases;

import com.myStore.pages.selectingPage;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class verifySelection {

    /*
    First Test Case
    In this Test Case we check the quick view and the info about the product
     */
    @Test
    public void verifySelectQuickView() throws InterruptedException {

        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php");

        //Create a selectingPage object and executes the method to test the quick view
        selectingPage selectQuickView = new selectingPage(driver);
        selectQuickView.clickOnQuickView();

        //Delay for verify the information
        Thread.sleep(5000);

        //Back to home page
        selectQuickView.backHomePage();

        //Quit the browser and print
        driver.quit();
        System.out.println("First Test Case: Completed");
    }

    /*
    First Test Case
    In this Test Case we check the More info button to get more information in a new page.
     */
    @Test
    public void verifySelectMoreInfo() throws InterruptedException {
        // as the firs scenario is the same setting
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php");

        //Create a selectingPage object and executes the method to test more info button
        selectingPage selectMoreInfo = new selectingPage(driver);
        selectMoreInfo.clickMoreInfo();

        //Delay for verify the information
        Thread.sleep(5000);

        //Back to home page
        selectMoreInfo.backHomePage();

        //Quit the browser and print the message
        driver.quit();
        System.out.println("Second Test Case: Completed");
        System.out.println("Second Scenario: Completed");
    }
}

