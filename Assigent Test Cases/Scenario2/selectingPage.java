/*
2045657 - Jose Jorge Aguado Martinez
This is the Main Test of the First and Second cases, in the first one is the test of the quick view for a window and
check the most relevant data of the product, the second one is about the more button for more information.
 */
package com.myStore.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class selectingPage {

    WebDriver driver;
    //Two elements in the web page, for quick view element and more info button
    By quickView = By.xpath("//*[@id=\"homefeatured\"]/li[1]/div/div[1]/div/div[1]/a");
    By moreInfo = By.xpath("//*[@id=\"homefeatured\"]/li[1]/div/div[2]/div[2]/a[2]");
    By backHome = By.xpath("//*[@id=\"columns\"]/div[1]/a[1]");

    //Constructor of the selectingPage
    public selectingPage(WebDriver driver){

        this.driver=driver;
    }

    //Method to click and see the quick view
    public void clickOnQuickView(){

        WebElement clickScriptView = driver.findElement(quickView);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", clickScriptView);
    }

    //Method to back to Home Page
    public void backHomePage(){

        WebElement backScriptView = driver.findElement(backHome);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", backScriptView);
    }

    //method to click and go to more info about the product
    public void clickMoreInfo(){

        WebElement clickScripInfo = driver.findElement(moreInfo);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", clickScripInfo);
    }
}
