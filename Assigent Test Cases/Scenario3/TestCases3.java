/*
2045657 - Jose Jorge Aguado Martinez
This is the Main Test of the First and Second cases, in the first one with a filter small size dresses
and the second one with a search with the keyword "printed"
 */
package com.TestCases;

import com.myStore.pages.filterPage;
import com.myStore.pages.selectingPage;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class verifyFilterPage {

    /*
    First Test Case
    In this Test Case the filter is a small size dress, in the checkbox "S", for Small
     */
    @Test
    public void verifyFilter() throws InterruptedException {

        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php?id_category=8&controller=category");

        //Create a filterPage object and executes one method to test the Dresses filter
        filterPage filter= new filterPage(driver);
        filter.setFilter();

        //Take a pause
        Thread.sleep(5000);

        //unset the same filter
        filter.setFilter();

        //Go back to home page
        filter.backHomePage();

        driver.quit();
        System.out.println("First Test Case: Completed");
    }

    /*
   Second Test Case
   In this Test Case we same the same setting in the web driver and web page. Also, in this test case we search for
   a keyword to match in the Dresses
    */
    @Test
    public void verifySearch() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php?id_category=8&controller=category");

        //Create a filterPage object and executes both methods setSearch for "printed" keyword and
        // then the clickSearchButton to executes the search, at the end quit the browser.
        filterPage search= new filterPage(driver);
        search.setSearch("Printed");
        search.clickSearchButton();

        //Take a pause
        Thread.sleep(5000);

        //Try an empty search and back to home page
        search.setSearch("");
        search.clickSearchButton();

        //Go back to home page
        search.backHomePage();

        driver.quit();

        System.out.println("Second Test Case: Completed");
        System.out.println("Third Scenario: Completed");
    }
}

