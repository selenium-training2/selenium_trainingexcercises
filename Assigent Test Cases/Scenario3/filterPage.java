package com.myStore.pages;
/*
2045657 - Jose Jorge Aguado Martinez
This is the Main Test of the First and Second cases, in the first one with a filter small size dresses
and the second one with a search with the keyword "printed"
 */
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class filterPage {

    WebDriver driver;

    //Three elements in the web page, filter checkbox, search input and the search button
    By filter = By.xpath("//*[@id=\"layered_id_attribute_group_1\"]");
    By search = By.xpath("//*[@id=\"search_query_top\"]");
    By searchButton = By.xpath("//*[@id=\"searchbox\"]/button");
    By backHome = By.xpath("//*[@id=\"columns\"]/div[1]/a[1]");

    //Constructor of the class
    public filterPage(WebDriver driver){

        this.driver=driver;
    }

    //Method to select the small size in the checkbox
    public void setFilter(){

        driver.findElement(filter).click();
    }

    //Method for enter the search
    public void setSearch(String research){

        driver.findElement(search).sendKeys(research);
    }

    //Method for click and search starts
    public void clickSearchButton(){

        driver.findElement(searchButton).click();
    }

    //Method to back to Home Page
    public void backHomePage(){

        WebElement backScriptView = driver.findElement(backHome);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", backScriptView);
    }

}

