public class threeLargest {

	public static void main(String[] args) {
		
		Scanner in=new Scanner(System.in);
		
		System.out.println("Please enter the size of the array:  ");
		int size=in.nextInt();
		int numbers[]= new int [size];
		System.out.println("Please enter the integer numbers in the array:  ");
		for(int i=0; i<numbers.length; i++) {
			int number=in.nextInt();
			numbers[i]=number;
		}
		int num1=0, num2=0, num3=0;
				
		for(int i=0; i<numbers.length; i++){
			if(numbers[i]>num1) {
				num1=numbers[i];
			}			
		}
		for(int i=0; i<numbers.length; i++){
			if(numbers[i]>num2 && numbers[i]<num1) {
				num2=numbers[i];
			}			
		}
		for(int i=0; i<numbers.length; i++){
			if(numbers[i]>num3 && numbers[i]<num2) {
				num3=numbers[i];
			}			
		}
		System.out.println("The largest numbers are:  "+num1+", "+num2+", "+num3);
		
	}

}
