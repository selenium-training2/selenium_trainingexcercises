//Jose Jorge Aguado Martinez 2045657
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

import java.util.List;

public class findElement {
    /*
        The main difference between findElement() and findElemnts is that,
         findElement is s command used to uniquely identify a web element within the web page,
         and findElements is used to identify a list of web elements in the web page.
         Another difference are:
         - findElement returns the first element that matches, and findElements return an array of all elements that matches.
         - With findElement we have a NoSuchElementException if the element is not found, in findEmlements only we have an empty list.
         */
    public static void main(String[] args)  {

        System.setProperty("webdriver.chrome.driver","C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        driver.get("https://formy-project.herokuapp.com/buttons");
//In this case we use the id, we inspect the page to find a button only one element, using the ID "btnGroupDrop1"
// Then click and next step
        WebElement oneElement=driver.findElement(By.id("btnGroupDrop1"));
        oneElement.click();

//In findElements we use a List to store the elements, we inspect in the page for elements with xpath //div
// Then we show them by the console with a for loop, then quit the browser and finish the test
        List<WebElement> listOfElements = driver.findElements(By.xpath("//div"));
        for(WebElement element:listOfElements)
        {
            System.out.println(element.getText());
        }
        driver.quit();
    }
}
