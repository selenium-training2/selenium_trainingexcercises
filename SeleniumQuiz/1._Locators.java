/*
Jose Jorge Aguado Martinez 2045657
*/

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
public class LocateSelenium {
        /*
        In Selenium We have several types of locators in one of them we can research the element we want to use for the test
        In this code we have some examples of its implementation.
         */
        public static void main(String[] args)  {

            System.setProperty("webdriver.chrome.driver","C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");

            WebDriver driver = new ChromeDriver();

            driver.get("https://formy-project.herokuapp.com/buttons");
//By Id is the most common using the id in HTML code of the element, we inspect the element in this case
// a button using its ID "btnGroupDrop1", then click and nex step of the test
            WebElement locatorId=driver.findElement(By.id("btnGroupDrop1"));
            locatorId.click();
//By Name is using the name, in HTML code of the element, we inspect the element in this case
// the autocomplete field using its name "q", we research the keyword "cat" then click and nex step of the test
            driver.get("https://www.google.com.mx/");
            WebElement locatorName=driver.findElement(By.name("q"));
            locatorName.sendKeys("Cat");
            locatorName.click();
//By ClassName is using the class name, in HTML code of the element, we inspect the element in this case
// a button using its classname "btn btn-lg btn-success",  then click and nex step of the test
            driver.get("https://formy-project.herokuapp.com/buttons");

            WebElement locatorClass=driver.findElement(By.className("btn btn-lg btn-success"));
            locatorClass.click();
            //By tagname is using the tag name, in HTML code of the element, we inspect the element in this case
            // a button using its tagname "tagNameButton",  then click and nex step of the test           
            WebElement locatorTag=driver.findElement(By.tagName("tagNameButton"));
            locatorTag.click();

            driver.get("https://www.google.com.mx/");
//By linkText is using the text of a link, in HTML code of the element, we inspect the element in this case
// a link "Espa�ol (Latinoamerica)",  then click and nex step of the test            
            driver.findElement(By.linkText("Espa�ol (Latinoam�rica)")).click();
//By PartialLink is using the first link in the page, in HTML code of the element, we inspect the element in this case
// a link using its text "Gmail",  then click and nex step of the test
            driver.findElement(By.partialLinkText("Gmail")).click();

            driver.get("https://formy-project.herokuapp.com/radiobutton");
//By CSS Selector is using Css, in Css and HTML code of the element, we inspect the element in this case
// a button using its css position,  then click and nex step of the test
            WebElement locatorCss=driver.findElement(By.cssSelector("input[value='option2']"));
            locatorCss.click();
            //By Xpath is using the path in HTML code of the element, we inspect the element in this case
            // a button using its  path in the HTML page "/html/body/div/div[3]/input", then click and nex step of the test           
            WebElement locatorXpath=driver.findElement(By.xpath("/html/body/div/div[3]/input"));
            locatorXpath.click();

            driver.quit();
        }
    }