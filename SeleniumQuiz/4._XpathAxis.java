//Jose Jorge Aguado Martinez 2045657
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

public class xpathAxis {
    /*
       An Xpath axis represents a relationship to the context (current) node,
       and is used to locate nodes relative to that node on the tree. There are many axis like ,ancestor, ancestor-or-self child,
       attribute, descendant, following, self. It helps in the location of an element in a node inside its tree.
       in Xpath with "/" we can separate this type of relationship or with "//" the children of that directory
       and with "@" the name of an attribute
     */
    public static void main(String[] args)  {

        System.setProperty("webdriver.chrome.driver","C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        driver.get("https://formy-project.herokuapp.com/buttons");

        //In this example we use the child of the div section to find the last element
        //Then enter a click and finish the test, we can see the use of the attribute with "@" and the // to enter a relative path

        WebElement pathAxis=driver.findElement(By.xpath("//div[last()]"));
        pathAxis.click();

        driver.quit();
    }
}
