//Jose Jorge Aguado Martinez 2045657
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class dropdown {
    /*
       In Selenium we can Select the elements by index or name in a menu, but we must add the import of the Select library
       then we need to create a Select object this will make as the object of the selections, and finally we can instance the selection
       in the menu by index with index or visible text.
     */
    public static void main(String[] args)  {

        System.setProperty("webdriver.chrome.driver","C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        driver.get("https://formy-project.herokuapp.com/buttons");

        //In this example we use the child of the div section to find the last element
        //Then enter a click and finish the test, we can see the use of the attribute with "@" and the // to enter a relative path

        Select dropdown = new Select(driver.findElement(By.id("btnGroupDrop1")));
        dropdown.selectByVisibleText("Dropdown link 1");
        dropdown.selectByIndex(2);

        driver.quit();
    }
}