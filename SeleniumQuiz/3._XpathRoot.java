//Jose Jorge Aguado Martinez 2045657
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

import java.util.List;

public class Xpath {
    /*
       The main difference between absolute and partial/relative Xpath is that,
       absolute Xpath uses complete path from the root element to the desire element like /html/body/div/div/...
       in another hand relative Xpath we can simply start by referencing the element we want and go from there like
       //div3
        */
    public static void main(String[] args)  {

        System.setProperty("webdriver.chrome.driver","C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();

        driver.get("https://formy-project.herokuapp.com/radiobutton");

        //In this example we use the absolute path in Xpath, to click on a ardio button,
        //we can see the entire root of the element

        WebElement absolutePath=driver.findElement(By.xpath("/html/body/div/div[3]/input"));
        absolutePath.click();

        //In this another example we use the relative path in Xpath, to click on a radio button,
        //we can see the relative root of the element since there are two slashes "//"

       WebElement relativePath=driver.findElement(By.xpath("//div[3]/input"));
        absolutePath.click();
        driver.quit();
    }
}