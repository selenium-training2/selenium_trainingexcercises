/*
            Jose Jorge Aguado Martinez 2045657 TCS
 */
package com.SeleniumQuizII;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class dragAndDrop {
     /* 2. Drag and Drop Implementation
       This action is performed using a mouse when a user moves (drags) a web element from one location and then
       places (drops) it at another point. Here, the user selects a file in the folder, drags it to the desired
       folder, and drops it. In Selenium we use the Action class to perfform this actions, we declare an object from this class
       then we have many methods to make actions, like dragAndDrop().
     */

    @Test
    public void verifyDragAndDrop(){

        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://formy-project.herokuapp.com/dragdrop");

        //Create a dragAndDropPage  object and executes the method to test
        dragAndDropPage test= new dragAndDropPage(driver);
        test.dragAndDropElements();
        //Quit the browser and finishes the Test
        driver.quit();
    }
}

class dragAndDropPage{

    WebDriver driver;
    //Elements in the web page
    By fromHere = By.xpath("//*[@id=\"image\"]");
    By toHere = By.xpath("//*[@id=\"box\"]");


    //Constructor of the class
    public dragAndDropPage(WebDriver driver) {

        this.driver = driver;
    }

    //Method for drag and drop an image
    public void dragAndDropElements() {
        WebElement drag=driver.findElement(fromHere);
        WebElement drop=driver.findElement(toHere);

        //Actions class and drag and drop method from that class: dragAndDrop(fromHere, toHere)
        Actions action=new Actions(driver);
        action.dragAndDrop(drag, drop).build().perform();
    }
}