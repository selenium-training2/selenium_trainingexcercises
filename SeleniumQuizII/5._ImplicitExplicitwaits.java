/*
            Jose Jorge Aguado Martinez 2045657 TCS
 */
package com.SeleniumQuizII;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class explicitAndImplicitWaits {
    /* 5. Which one is prefered Implicit or Explicit Wait
    Implicit waits are used to provide a default waiting time (like 30 seconds) between each consecutive test
    step/command across the entire test script, the subsequent test step would only execute when the 30
    seconds have ended and then passes to next step of the test.
    The implicit wait is a single line of a code and can be declared in the setup method of the test script.
    Also, implicit wait is transparent and uncomplicated with simpler syntax than explicit wait.
    drv.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    Explicit waits are used to stop the execution until the time a particular condition is met or the maximum time
    has ended. Explicit waits are applied for a particular instance only.WebDriver introduces classes like WebDriverWait
    and ExpectedConditions to enforce Explicit waits into the test scripts.
    WebDriverWait wait = new WebDriverWait(wait,30);
    wait.util(ExpectedConditions.visibilityOfElementLocated(By.className("element")));


    There is no better than the other type of wait, each scenario have its best option, explicit or implicit wait
    In the Scenario in which we want to wait for some element, the best option is Explicit waits, if we only want to
    wait for a moment to see some information is better an Implicit wait.
    Example:
    Scenario is that we have to enter the credential in a login page, the best option is Implicit wait this is for wait
    to see the correct login in the page.
    */


    @Test
    public void verify(){

            //Setting the web driver and web page
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
            WebDriver driver=new ChromeDriver();
            driver.manage().window().maximize();
            driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");

            //Create a loginPage object and executes three methods to test the valid login
            implicitWaitPage login= new implicitWaitPage(driver);
            login.typeEmail("jorge.aguado.martinez@gmail.com");
            login.typePassword("Demo123");

            //Delay with implicit wait to see the info
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            login.clickOnLoginButton();

            //Delay for see the login
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            //Quit the browser and finishes the Test
            driver.quit();
        }

    }


class implicitWaitPage{
    WebDriver driver;
    //Three elements in the web page, username/email, password and the login button
    By emailAddress = By.id("email");
    By password = By.id("passwd");
    By loginButton = By.id("SubmitLogin");

    //Constructor of the class
    public implicitWaitPage(WebDriver driver){

        this.driver=driver;
    }

    //Method for enter the email address
    public void typeEmail(String email){

        driver.findElement(emailAddress).sendKeys(email);
    }

    //Method for enter a password
    public void typePassword(String passwd){

        driver.findElement(password).sendKeys(passwd);
    }

    //Method for login in the Account
    public void clickOnLoginButton(){

        driver.findElement(loginButton).click();
    }
}
