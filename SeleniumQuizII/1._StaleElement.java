/*
            Jose Jorge Aguado Martinez 2045657 TCS
 */

package com.SeleniumQuizII;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class staleElements {

    /* 1. StaleElementsException
       Stale Element means an old element.Selenium keeps a track of all elements in form of a reference as and when
       findElement/findElements is used, selenium uses that reference instead of finding the element again.
       But sometimes due the AJAX request and responses this reference is no longer fresh,
       then the StaleElementReferenceException is thrown. We can handle this Exception throw refreshing the page,
        handling its reference of the Element, wait for the element or a try/catch block.
        By refreshing is
            driver.navigate().refresh();
            driver.findElement(By.xpath("")).click();
        By try/catch block
            // Using for loop, it tries for 3 times.
            // If the element is located for the first time then it breaks from the for loop nad comeout of the loop
            for(int i=0; i<=2;i++){
                try{
                      driver.findElement(By.xpath("xpath here")).click();
                      break;
                       }
               catch(Exception e){
               System.out(e.getMessage());
                 }
               }
        By expected condition
        wait.until(ExpectedConditions.refreshed(ExpectedConditions.stalenessOf("table")));
     */

    @Test
    public void verifyButton(){

        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://formy-project.herokuapp.com/buttons");

        //Create a stealElementExceptionHandle object and executes the method to test
        staleElementExceptionHandle clickPrimary= new staleElementExceptionHandle(driver);

        //Here we refresh the page
        driver.navigate().refresh();
        clickPrimary.clickOnButton();

        //Quit the browser and finishes the Test
        driver.quit();
    }

}

class staleElementExceptionHandle {

    WebDriver driver;
    //Elements in the web page, a button
    By primaryButton = By.xpath("/html/body/div/form/div[1]/div/div/button[1]");


    //Constructor of the class
    public staleElementExceptionHandle(WebDriver driver) {

        this.driver = driver;
    }

    //Method for click Button
    public void clickOnButton() {

        driver.findElement(primaryButton).click();
    }
}