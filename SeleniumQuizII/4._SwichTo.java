/*
            Jose Jorge Aguado Martinez 2045657 TCS
 */
package com.SeleniumQuizII;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class switchTo {
    /* 4. Explain 3 Scenarios in which driver.switchTo() method can be used
    SwitchTo() is a method for changes or navigate to many windows, Popups or pages
        1. If we open a new window we can use switchTo(), to go back to the parent page.
        2. We can close a window that pops up like an alert or warning
        3. If the test is too long we can go to one page for a test, and then go to another page for another test

    */

    @Test
    public void verifySwitchTo() throws InterruptedException {

        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://formy-project.herokuapp.com/switch-window");

        //Create a switchToPage object and executes the method to test
        switchToPage test= new switchToPage(driver);

        //We use the method for navigate
        test.newTab();

        //Quit the browser and finishes the Test
        driver.quit();
    }
}
class switchToPage{
    WebDriver driver;
    //Elements in the web page
    By tabButton = By.xpath("//*[@id=\"new-tab-button\"]");



    //Constructor of the class
    public switchToPage(WebDriver driver) {

        this.driver = driver;
    }

    //Method for new tab in the window
    public void newTab() throws InterruptedException {
        //We create a new tab
        driver.findElement(tabButton).click();
        //Get the control of the window
        String originalHandle=driver.getWindowHandle();

        //A for loop for changes the window
        for(String handle1:driver.getWindowHandles()){
            driver.switchTo().window(handle1);
            Thread.sleep(5000);
        }
        //Back to the Original Page
        driver.switchTo().window(originalHandle);

    }
    }