/*
            Jose Jorge Aguado Martinez 2045657 TCS
 */
package com.SeleniumQuizII;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class closeAndQuit {
    /* 3. Difference between close and quit
        The main difference is what closes the method. In the first one closes only the test page, and the second one the whole Webdriver
        Close() It is used to close the browser or page currently which is having the focus, The Webdriver is still opens
        Quit() It is used to shut down the web driver instance or destroy the web driver instance(Close all the windows),
        and ends the WebDriver session gracefully.
    */

    @Test
    public void verifyQuit(){
        /*
        In this test with quit() method we open 2 windows and the closes both of them
         */

        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://formy-project.herokuapp.com/switch-window");

        //Create a dragAndDropPage  object and executes the method to test
        closeAndQuitPage test= new closeAndQuitPage(driver);
        test.newTabButton();

        //Quit the browser and finishes the Test
        driver.quit();
    }

    @Test
    public void verifyClose(){
        /*
        In this test we use close() method open 2 windows and only closes the test page (the first one)
         */

        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://formy-project.herokuapp.com/switch-window");

        //Create a closeAndQuitPage object and executes the method to test
        closeAndQuitPage test= new closeAndQuitPage(driver);
        test.newTabButton();

        //Closes the page only
        driver.close();
    }
}

class closeAndQuitPage{
    WebDriver driver;
    //Elements in the web page
    By tabButton = By.xpath("//*[@id=\"new-tab-button\"]");



    //Constructor of the class
    public closeAndQuitPage(WebDriver driver) {

        this.driver = driver;
    }

    //Method for new tab in the window
    public void newTabButton() {
        driver.findElement(tabButton).click();

    }

}