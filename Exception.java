/*
 * Try...catch in java are the keywords to handle exception, a exception is an error that occurs during the normal execution 
 * of a program. If the exception cannot be handled then the execution stops and finishes the program. 
 * In java with the try keyword declares a section of code that could fail or throw a exception, 
 * the catch keyword is for handle that exception and notice, so this syntaxes make possible throws the exception to the user
 * 
 */
public class ExceptionExample {

	public static void main(String[] args) {
		/*
		 * In this example there is an array of one number then we try to assign a value in the 23th position
		 * but the array is initialized with a single number, so it will throw an exception, 
		 * this is a ArrayIndexOutOfBoundsException caused by index of the array.
		 */
		 try {  
	            int array[] = {2};  
	            array[25] = 777; 
	        } 
		 //We wrapped the exception and show a massage for the user
		 catch (ArrayIndexOutOfBoundsException e) {  
	        System.out.println("Stop caused by  Exception: " + e); 
	    } 
	    System.out.println("End of the program");

	}
