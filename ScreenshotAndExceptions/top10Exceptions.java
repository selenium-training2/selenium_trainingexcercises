package com.Selenium.Screeshot;

import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.concurrent.TimeUnit;

public class top10Exceptions {
    /*Exceptions are events that java program ends abruptly without giving expected output.
    Java provides a framework where a user can handle exceptions, this is by try-catch block
    In selenium Webdriver there are many types of exceptions, added to the Java exceptions.
    So here are 10 most common exceptions in Selenium Webdriver and an example of them.
     */

    /*
    10. StaleElemets Exception
      Stale Element means an old element.Selenium keeps a track of all elements in form of a reference as and when
       findElement/findElements is used, selenium uses that reference instead of finding the element again.
       We can handle this Exception throw refreshing the page,handling its reference of the Element,
       wait for the element or a try/catch block.
     */
    @Test
    public void staleElementException(){
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");

        // Here if the element is located for the first time then it breaks from the for loop and comes out of the loop
        // otherwise try again.
        for(int i=0; i<=2;i++){
            try{
                driver.findElement(By.cssSelector("#menu-maxmegamenu > li.search-item.menu-item-align-right > a > span > svg:nth-child(1) > path"));
                break;
            }
            catch(StaleElementReferenceException e){

            }
        }
        driver.close();
    }
    /*
    9. NoSuchSessionException
      This exception is when a method is called after quitting the browser by WebDriver.quit().
      This can also happen due to web browser issues like crashes and WebDriver cannot execute any command
      using the driver instance.
       We can handle this Exception throw avoiding to use quit() before testing all the page or tests,
       another way to handle it is a try/catch block.
     */
    @Test
    public void noSuchSessionException(){
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");

        //We quit the browser and then try to pass a line of code finding a webelement
        driver.quit();
            //This block handle the exception
            try{
                driver.findElement(By.cssSelector("#menu-maxmegamenu > li.search-item.menu-item-align-right > a > span > svg:nth-child(1) > path"));
                }
            catch(NoSuchSessionException e){

            }
        }

    /*
    8.TimeOutException
  This exception happens when a command completion takes more than the wait time. Waits are mainly used in WebDriver
  to avoid the exception ElementNotVisibleException. If WebDriver tries to find an element before the
  page completely loads, then exception ElementNotVisibleException is thrown. To avoid this exception, waits commands are added.
  If the components don’t load even after the wait, the exception org.openqa.selenium.TimeoutException will be thrown.

   We can handle this Exception throw using explicit waits with java executor, or we can tune the time to load the webpage
   another way to handle it is a try/catch block.
 */
    @Test
    public void timeoutException(){
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        try {
            //Here we give only 1 sec to load the web page an then if does not work we use the try-catch block
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);//Here we can tune the wait for the page
            driver.get("https://www.youtube.com/");
        }catch(TimeoutException e){

        }
        driver.close();
    }
    /*
    7.ElementNotSelectableException
     THis Exception occurs under InvalidElementStateException class. ElementNotSelectableException indicates that
     the web element is present in the web page but cannot be selected. We can check if the element is or not clickable.

   We can handle this Exception throw using explicit/implicit waits until the element can be clickable,
   another way to handle it is a try/catch block.
 */
    @Test
    public void elementNoSelectableException(){
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");

        try {
            //Here we try to click an element no clickable and then the try-catch block wraps the exception
            WebElement test =driver.findElement(By.xpath("//*[@id=\"countries\"]/tbody/tr[2]/td[2]/strong"));
            test.click();
        }catch(ElementNotSelectableException e){
        }
        driver.close();
    }

    /*
   6. ElementNotVisibleException
    This exception is thrown when WebDriver tries to perform an action on an invisible web element,
    which cannot be interacted with. That is, the web element is in a hidden state, so it's not possible to use it.

    We can handle this Exception throw using explicit/implicit waits until the element can be visible,
    another way to handle it is a try/catch block, like others exceptions.
*/
    @Test
    public void elementNoVisibleException(){
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");

        try {
            //Here we try to click an element, waits 10 sec to be visible and then the try-catch block wraps the exception
            WebElement menu =driver.findElement(By.xpath("//*[@id=\"menu-item-4420\"]/a"));
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            menu.click();
        }catch(ElementNotVisibleException e){
        }
        driver.close();
    }

    /*
    5. InvalidSelectorException
   This occurs when a selector is incorrect or syntactically invalid.
   This exception happens commonly when XPATH locator is used.

     We can handle this Exception throw using explicit/implicit waits until the element can be visible,
    another way to handle it is a try/catch block, like others exceptions.
*/
    @Test
    public void invalidSelectorException(){
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");

        try {
            //Here we try to locate an element, then the try-catch block wraps the exception because the Xpath is wrong
            WebElement menu = driver.findElement(By.xpath("//*[@id=\"menu-item-4420\"]a"));
        }catch(InvalidSelectorException e){

        }

        driver.close();
    }
    /*
  4. NoAlertPresentException
   NoAlertPresentException under NotFoundException is thrown when WebDriver tries to switch to an alert,
   which is not available.
    We can handle this Exception throw using explicit waits only until the alert,
   another way to handle it is a try/catch block, like others exceptions.
*/
    @Test
    public void noAlertPresentException(){
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");

        try {
            //Here we create a explicit wait  for the alert, then the try-catch block wraps the exception
            Alert alert;
            WebDriverWait wait=new WebDriverWait(driver, 10);
            alert=driver.switchTo().alert();
            alert.accept();
            wait.until(ExpectedConditions.alertIsPresent());

        }catch(NoAlertPresentException e){

        }

        driver.close();
    }

    /*
     3. NoSuchFrameException
    When WebDriver is trying to switch to an invalid frame, NoSuchFrameException under NotFoundException class is thrown.
    This is for no existing frame or not found.
    We can handle this Exception throw using explicit waits only until the frame,
    another way to handle it is a try/catch block, like others exceptions.
*/
    @Test
    public void noSuchFrameException(){
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");

        try {
            //Here we create a explicit wait  for the frame, then the try-catch block wraps the exception
            WebDriverWait wait=new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("No exists"));
            //this frame doesn't exist so this causes the exception
            driver.switchTo().frame("No exists");

        }catch(NoSuchFrameException e){

        }
        catch(TimeoutException e){

        }

        driver.close();
    }

    /*
     2. NoSuchWindowException
    NoSuchWindowException comes when WebDriver tries to switch to an invalid window.
    Also, the window handle doesn’t exist or is not available to switch.
    We can handle this Exception throw using explicit waits only until the window,
    another way to handle it is a try/catch block, like others exceptions.
*/
    @Test
    public void noSuchWindowException() {
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");

        for (String handle : driver.getWindowHandles()) {
            try {
                //Here we try to navigate but there is no added window
                driver.switchTo().window(handle);
            } catch (NoSuchWindowException e) {

            }
        }
        driver.close();
    }

    /*
     1. NoSuchElementException
    This exception occurs when WebDriver is unable to find and locate elements.
    Usually, this happens when tester writes incorrect element locator in the findElement(By, by) method.
    We can handle this Exception throw using explicit/implicit waits for the element ,
    another way to handle it is a try/catch block, like others exceptions.
*/
    @Test
    public void noSuchElementException() {
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");

        WebElement tesElement= driver.findElement(By.className("hasVisite"));
            try {
                //Here we crate a new element to find in the webpage, but the class name is wrong
                //Te try - catch wraps the exception
                WebElement testElement= driver.findElement(By.className("hasVisite"));//the correct class name is "hasVisited"
            } catch (NoSuchElementException e) {
            }
        driver.close();
    }

    }



