package com.Selenium.Screeshot;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class screenshotPage {
    WebDriver driver;


    @Test
    public void verifyScreenshot() throws IOException {
        //Setting the web driver and web page
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");

        partialScreenshot("C:\\Users\\JoseJorge\\Desktop\\PartialScreenshot.png");
        fullScreenshot("C:\\Users\\JoseJorge\\Desktop\\fullScreenshot.png");
        elementScreenshot("C:\\Users\\JoseJorge\\Desktop\\elementScreenshot.png", "//*[@id=\"site-navigation\"]/div/button");

        //Quit the webdriver
        driver.quit();
    }

    public void partialScreenshot(String path){

        //Take the screenshot
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        //Copy the file to a location and use try catch block to handle exception
        try {
            FileUtils.copyFile(screenshot, new File(path));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void fullScreenshot(String path) throws IOException {

        // Capture full screenshot and storage in the path
        Screenshot s=new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
        ImageIO.write(s.getImage(),"PNG",new File(path));

    }

    public void elementScreenshot(String path, String xpathElement) throws IOException {
        // Locate the element on the web page by xpath
        WebElement element = driver.findElement(By.xpath(xpathElement));

        // Get screenshot of the visible part of the web page
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        // Convert the screenshot into BufferedImage
        BufferedImage fullScreen = ImageIO.read(screenshot);

        //Find location of the webelement logo on the page
        Point location = element.getLocation();

        //Find width and height of the located element logo
        int width = element.getSize().getWidth();
        int height = element.getSize().getHeight();

        //Full image to get only the logo screenshot
        BufferedImage logoImage = fullScreen.getSubimage(location.getX(), location.getY(),
                width, height);
        ImageIO.write(logoImage, "png", screenshot);

        //Save cropped Image at destination location physically.
        FileUtils.copyFile(screenshot, new File(path));


    }

}
