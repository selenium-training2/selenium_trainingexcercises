package com.Selenium.DataDriven;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class DDTExcel {
     ChromeDriver driver;

    @Test(dataProvider="testData")
    public void DemoProject(String email, String password){
        //Browser setting
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        WebDriver driver;
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");

        //Web element for the test
        driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(email);
        driver.findElement(By.xpath("//*[@id=\"passwd\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"SubmitLogin\"]")).click();

        //Implicit wait for the credential in the login page
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        Assert.assertTrue("Invalid credentials", driver.getTitle().matches("Logging to the App:"));
        System.out.println("Login successful");

    }

    @AfterMethod
    void programTermination(){
        driver.quit();

    }

    @DataProvider(name="testData")
    public Object[][] testDataFeed(){
        ReadExcelFile login=new ReadExcelFile("C:\\Users\\JoseJorge\\Desktop\\ExcelDD.xlsx");
        int rows =login.getRowCount(0);
        Object[][] credentials =new Object[rows][2];

        //Here we use a try catch to prevent an Exception caused by size of the array
        try {
            for (int i = 0; i < rows; i++) {
                credentials[i][0] = login.getData(0, i, 0);
                credentials[i][1] = login.getData(0, i, 1);
            }
        }catch(Exception e){

        }
        return credentials;
    }
}
