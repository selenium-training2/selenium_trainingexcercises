package com.Selenium.DataDriven;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;

public class ReadExcelFile {
    XSSFWorkbook wb;
    XSSFSheet sheet;

    public ReadExcelFile(String excelPath){
        try{
            File src =new File(excelPath);
            FileInputStream fis=new FileInputStream(src);
            wb=new XSSFWorkbook();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    //Get Data from a specific Excel sheet, row and column
    public String getData(int sheetNumber, int row, int column){
        sheet=wb.getSheetAt(sheetNumber);
        String data =sheet.getRow(row).getCell(column).getStringCellValue();
        return data;
    }

    //Counter of rows in the sheet
    public int getRowCount(int sheetIndex){
        int row=wb.getSheetAt(sheetIndex).getLastRowNum();
        row+=1;
        return row;
    }
}
