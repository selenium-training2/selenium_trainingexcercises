package com.Selenium.ExcelWebTable;


import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;

public class verifyWebTable {

    WebDriver driver;
    WriteWebTable writeFile;
    ReadWebTable readFile;

    By countryRow = By.xpath("//*[@id=\"countries\"]/tbody/tr[1]/td[2]");
    By capitalRow= By.xpath("//*[@id=\"countries\"]/tbody/tr[1]/td[3]");
    By currencyRow= By.xpath("//*[@id=\"countries\"]/tbody/tr[1]/td[4]");
    By languageRow = By.xpath("//*[@id=\"countries\"]/tbody/tr[1]/td[5]");


    @Test
    public void test() throws IOException {
        String pathFile = "C:\\Users\\JoseJorge\\Desktop\\TestWebTable.xlsx";

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\JoseJorge\\Downloads\\chromedriver.exe");
        driver = new ChromeDriver();
        writeFile = new WriteWebTable(pathFile);
        readFile = new ReadWebTable(pathFile);

        driver.get("https://cosmocode.io/automation-practice-webtable/");

        WebElement testTable=driver.findElement(By.cssSelector("#countries"));

        int rows=readFile.readSizeRows();

        setTable(rows, testTable);

        driver.quit();
    }

   public void setTable(int rows, WebElement testTable) throws IOException {

       writeFile.writeExcelTable();
       int cells=readFile.readSizeCells(rows);
        for(int i=0;i<cells;i++)
        {

            String country=testTable.findElement(countryRow).getText();
            writeFile.writeCellText(i, 0, country);

            String capital=testTable.findElement(capitalRow).getText();
            writeFile.writeCellText( i, 1, capital);

            String currency=testTable.findElement(currencyRow).getText();
            writeFile.writeCellText( i, 2, currency);

            String language=testTable.findElement(languageRow).getText();
            writeFile.writeCellText( i, 3, language);


        }

    }
}

