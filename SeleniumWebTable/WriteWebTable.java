package com.Selenium.ExcelWebTable;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class WriteWebTable {

    String pathFile;
    public String sheetName="Hoja1";
    public WriteWebTable(String path){
        this.pathFile=path;
    }
    public void writeExcelTable() throws IOException {

        File file = new File (pathFile);
        FileInputStream input= new FileInputStream(file);

        XSSFWorkbook newWorkbook = new XSSFWorkbook(input);

        XSSFSheet CountrySheet = newWorkbook.getSheet(sheetName);
        int rowCounter = CountrySheet.getLastRowNum()-CountrySheet.getFirstRowNum();
        XSSFRow row = CountrySheet.getRow(0);
        XSSFRow newRow = CountrySheet.createRow(rowCounter+1);
        input.close();

    }

    public void writeCellText(int row, int cell, String text) throws  IOException {
        File file = new File(pathFile);
        FileInputStream input= new FileInputStream(file);
        XSSFWorkbook newWorkbook = new XSSFWorkbook(input);
        XSSFSheet newSheet = newWorkbook.getSheet(sheetName);
        XSSFRow rowWrite = newSheet.getRow(row);


        FileOutputStream outFile=new FileOutputStream(pathFile);
        newWorkbook.write(outFile);
        newSheet=newWorkbook.createSheet(sheetName);
        XSSFRow newRow = newSheet.createRow(row);
        XSSFCell writeCell = rowWrite.createCell(cell);

        writeCell.setCellValue(text);
        input.close();
    }

}
