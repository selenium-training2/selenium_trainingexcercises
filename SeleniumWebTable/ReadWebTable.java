package com.Selenium.ExcelWebTable;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ReadWebTable {

    String pathFile;
    public String sheetName ="Hoja1";
    public ReadWebTable(String path){
        this.pathFile=path;
    }

    public int readSizeRows() throws IOException {
        File file = new File(pathFile);
        FileInputStream inputXls = new FileInputStream(file);

        XSSFWorkbook newWorkbook = new XSSFWorkbook(inputXls);
        XSSFSheet CountrySheet = newWorkbook.getSheet(sheetName);

        int rowCount = CountrySheet.getLastRowNum() - CountrySheet.getFirstRowNum();

        return rowCount;

        }

    public int readSizeCells(int row) throws IOException {
        XSSFRow rowCount;

        File file = new File(pathFile);
        FileInputStream inputXls = new FileInputStream(file);

        XSSFWorkbook newWorkbook = new XSSFWorkbook(inputXls);
        XSSFSheet CountrySheet;

        CountrySheet=newWorkbook.getSheet(sheetName);
        rowCount=CountrySheet.getRow(row);
        int cellCounter = rowCount.getLastCellNum() - rowCount.getFirstCellNum();

        return cellCounter;

    }


    public String getCellText(int row, int cell) throws IOException {
        File file = new File(pathFile);
        FileInputStream input = new FileInputStream(file);

        XSSFWorkbook newWorkbook = new XSSFWorkbook(input);
        XSSFSheet CountrySheet = newWorkbook.getSheet(sheetName);

        XSSFRow rowText= CountrySheet.getRow(row);
        XSSFCell cellText = rowText.getCell(cell);
        return cellText.getStringCellValue();

    }

}
