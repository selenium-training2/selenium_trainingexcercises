/*
 * Inheritance means that one class (sub class) have properties, methods and data from another class (super class)
 * In this example there are three classes, Transport is the superclass and Car and Taxi Classes are the subclasses
 * This two have different cost for transportation, taxis is a cost per Km and Cars is for cost in the gasoline
 * Here we declare the Id, Model of every car.
 */

public class Transport {
	private int id;
	private String model;
	
	public Transport(int id, String model) {
		this.id = id;
		this.model = model;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	
}
/*
 * This class is child from Transport class, in java this is with the keywords "extends" and "super"
 * Here only we declare the gas cost and inherits the another arguments from the Transpor class 
 */
public class Car extends Transport{
	private double gasCost;
	
	//Right here we see the argumenst from transport class
	public Car(int id, String model, double gasCost) {
		super(id, model);
		this.gasCost=gasCost;
	}

	public double getGasCost() {
		return gasCost;
	}

	public void setGasCost(double gasCost) {
		this.gasCost = gasCost;
	}

	
}

/*
 * As the Car class Taxi class have the same form and inherits arguments from the Transport class
 * We only declare the Price per Km
 */
public class Taxi extends Transport{
	private double price;

	public Taxi(int id, String model, double price) {
		super(id, model);
		this.price=price;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	
}
/*
 * In the main we declare two objects one of the Car class and one of the Taxi class
 * Printing the information we can see that with Inheritance we can use less code, 
 * and describe objects of the real world.
 */
public class MainTransport {

	public static void main(String[] args) {
		Car c1=new Car(347, "M78CD", 7.8);
		System.out.println("Car with id:"+c1.getId()+" Model: "+c1.getModel()+" Gas Cost: "+c1.getGasCost());
       
		Taxi t1=new Taxi(567, "KO90L", 45.9);
		System.out.println("Taxi with id:"+t1.getId()+" Model: "+t1.getModel()+" Price per Km: "+t1.getPrice());
	}

}

