/*
 * Polymorphism is the ability to take different forms or to do different tasks
 * in java we have polymorphism in the syntaxes when we create several methods with the same name,
 * if the methods have different returns or different parameters then the method is overloading.
 * when the method is from a sub class that implements the same method of the superclass then is overrriding the method
 * In java we can java both types of polymorphism.
 * 
 * In this example there is a class call Student that takes a student with name, age an d his/her mark in a game
 * 
 */
public class Student {
	private String name;
	private double markInGame;
	private int age;
	
	/*
	 * Constructor with the markInGame =0 initial score in the game
	 */
	public Student(String name, double markInGame, int age) {
		this.name = name;
		this.markInGame = markInGame;
		this.age = age;
		markInGame=0.0;
	}
//Getters and setters of the arguments 
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMarkInGame() {
		return markInGame;
	}

	public void setMarkInGame(double markInGame) {
		this.markInGame = markInGame;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
/*
 * Both methods have the same name and both are void, but the second one have two parameters
 * This First Method add the last score to the actual score
 * The second method do the same and hace a counter of the lives that this student had used in the game 
 */
	public void add(double marks) {
		this.markInGame+=marks;
	}
	public int add(double marks, int times) {
		
		this.markInGame+=marks;
		times+=times;
		return times;
	}
	
}

public class Main {

	public static void main(String[] args) {
		//Maria is a student and played for the first time
		//Here the add method only shows the Maria' Score
		Student maria=new Student("Maria", 0.0, 12);
		maria.add(6.90);
		System.out.println("Score: "+maria.getMarkInGame()+"  Age: "+maria.getAge());
		/*
		 * Then she played again and used 2 lives with a score of 1.90
		 * but she did not show her progress
		 * Here is the polymorphism with the another add method 
		 */
		maria.add(1.90, 2);
		//And the last time she played she used 3 lives and show every data this time
		//Finally we can see the second add method and its return, different from the first add method
		System.out.println("Score: "+maria.getMarkInGame()+"  Age: "+maria.getAge()+"  lives used: "+maria.add(6.77, 3));
		
	}

}
